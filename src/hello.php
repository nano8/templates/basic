<?php

use laylatichy\nano\core\request\Request;
use laylatichy\nano\core\response\Response;

useRouter()->get('/', fn (): Response => useResponse()
    ->withJson([
        'hello' => 'world!',
    ]));

useRouter()->get('/{name}', fn (Request $request, string $name): Response => useResponse()
    ->withJson([
        'hello' => $name,
    ]));