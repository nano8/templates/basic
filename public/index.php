<?php

require_once __DIR__ . '/../vendor/autoload.php';

useConfig()->withRoot(__DIR__ . '/../src');

useNano()
    ->start();
